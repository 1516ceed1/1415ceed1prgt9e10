/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo7Alumnogrupo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;

/**
 * @date 14-abr-2015 Fichero Main.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        ObjectContainer bd;
        String nombrefichero = "Ejemplo7Alumnogrupo.db4o";
        File f = new File(nombrefichero);

        bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);

        // Almacena Alumnos
        Grupos g1 = new Grupos("g1", "grupo1");
        Alumnos a1 = new Alumnos("a1", "alumno1", g1);
        bd.store(a1);

        Grupos g2 = new Grupos("g2", "grupo2");
        Alumnos a2 = new Alumnos("a2", "alumno2", g1);
        bd.store(a2);

        System.out.println("Recuperado objetos alumno");

        Grupos g0 = new Grupos("g1", null);
        Alumnos a0 = new Alumnos(null, null, g0);
        ObjectSet res = bd.queryByExample(a0);

        System.out.println("Objetos alumno recuperados: " + res.size());
        while (res.hasNext()) {
            System.out.println(res.next()); //toString
        }
        bd.close();
    }

}
